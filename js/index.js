// EXCECISE 1
var ex1Btn = document.getElementById('ex1__btn');

ex1Btn.addEventListener('click', function () {
  var sum = 0;
  var n = 0;
  while (sum < 10000) {
    n++;
    sum += n;
  }

  document.getElementById('ex1__result').innerHTML = `Số nguyên dương nhỏ nhất: ${n}`;
});

// EXERCISE 2
var ex2Btn = document.getElementById('ex2__btn');

ex2Btn.addEventListener('click', function () {
  var numberX = document.getElementById('ex2__x').value * 1;
  var numberN = document.getElementById('ex2__n').value * 1;
  var sum = 0;

  for (var i = 1; i <= numberN; i++) {
    sum += Math.pow(numberX, i);
  }
  document.getElementById('ex2__result').innerHTML = `Tổng: ${sum}`;
});

// EXERCISE 3
var ex3Btn = document.getElementById('ex3__btn');

ex3Btn.addEventListener('click', function () {
  var number = document.getElementById('ex3__num').value * 1;
  var factorial = 1;

  for (var i = 1; i <= number; i++) {
    factorial *= i;
  }
  document.getElementById('ex3__result').innerHTML = `Tổng: ${factorial}`;
});

// EXERCISE 4
var ex4Btn = document.getElementById('ex4__btn');
var result = document.getElementById('content');

ex4Btn.addEventListener('click', function () {
  var contentHTML = '';

  result.style.display = 'block';

  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      contentHTML += `<div class="red">Div chẵn ${i}</div>`;
    } else if (i % 2 != 0) {
      contentHTML += `<div class="blue">Div lẻ ${i}</div>`;
    }
    result.innerHTML = contentHTML;
  }
});
